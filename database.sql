DROP TABLE IF EXISTS person_livre;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS livre;


CREATE TABLE
    person (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        firstname VARCHAR(100) NOT NULL
    );


CREATE TABLE
    livre(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        breed VARCHAR(255),
        birthdate DATE
    );

CREATE TABLE
    address (
        id INT PRIMARY KEY AUTO_INCREMENT,
        number VARCHAR(8),
        street VARCHAR(255),
        city VARCHAR(100),
        zip_code VARCHAR(6),
        id_person INT,
        FOREIGN KEY (id_person) REFERENCES person(id) ON DELETE SET NULL
    );

    
CREATE TABLE person_livre (
    id_livre INT,
    id_person INT,
    PRIMARY KEY (id_livre,id_person),
    Foreign Key (id_livre) REFERENCES livre(id) ON DELETE CASCADE,
    Foreign Key (id_person) REFERENCES person(id) ON DELETE CASCADE
    
);




INSERT INTO person (name,firstname) VALUES 
('name1', 'firstname1'),
('name2', 'firstname2'),
('name1', 'firstname3');

INSERT INTO livre (name,breed,birthdate) VALUES 
('Fido','Corgi', '2021-04-24'), 
('Rex', 'Daschund', '2019-10-15'), 
('Jean Marc', 'German Shepard', '2021-07-28'), 
('Fido', 'Poodle', '2010-01-05'), 
('Max', 'Corgi', '2018-04-01');

INSERT INTO
    address (number,street,city,zip_code,id_person)
VALUES 
    ('146','rue antoine primat','Villeurbanne','69100',1);


INSERT INTO person_livre (id_livre,id_person) VALUES (1, 1), (1, 3), (2, 1), (2, 2);
