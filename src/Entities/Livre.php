<?php

namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;


class Livre {
    private ?int $id;
	private ?string $titre;
    private ?string $auteur;
	private ?int $dispo;



	
	public function __construct(?string $titre, ?string $auteur, ?int $dispo = 1,?int $id = null) {
		$this->id = $id;
		$this->titre = $titre;
		$this->auteur = $auteur;
		$this->dispo = $dispo;
	
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitre(): ?string {
		return $this->titre;
	}
	
	/**
	 * @param string|null $titre 
	 * @return self
	 */
	public function setTitre(?string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuteur(): ?string {
		return $this->auteur;
	}
	
	/**
	 * @param string|null $auteur 
	 * @return self
	 */
	public function setAuteur(?string $auteur): self {
		$this->auteur = $auteur;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getDispo(): ?int {
		return $this->dispo;
	}
	
	/**
	 * @param int|null $dispo 
	 * @return self
	 */
	public function setDispo(?int $dispo): self {
		$this->dispo = $dispo;
		return $this;
	}
	
}